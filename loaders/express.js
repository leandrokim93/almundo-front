const BodyParser = require("body-parser");
const Cors = require("cors");

module.exports = async ({ app }) => {
    app.get("/status", (req, res) => { res.status(200).end(); });

    app.use(Cors());
    app.use(BodyParser.urlencoded({ extended: false }));

    app.use("/hotels", require("../api/hotelApi"));
    app.use("/", require("../api/homePageApi"));

    console.log("Express Initialized");
    
    return;
};