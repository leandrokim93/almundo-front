const HotelDatabase = require("../database/hotelDatabase");

module.exports.getHotels = (query) => {
    const hotels = HotelDatabase.getHotels();

    const filteredHotels = hotels.filter((hotel) => {
        return hotel.matchesQuery(query);
    });

    return filteredHotels;
};

module.exports.createHotel = (hotel) => {
    return HotelDatabase.createHotel(hotel);
};

module.exports.updateHotel = (hotel) => {
    return HotelDatabase.updateHotel(hotel);
};

module.exports.deleteHotel = (hotel) => {
    return HotelDatabase.deleteHotel(hotel);
};