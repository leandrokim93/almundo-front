function Hotel(hotel) {
    this.id = hotel.id;
    this.name = hotel.name;
    this.stars = hotel.stars;
    this.price = Math.round(hotel.price);
    this.image = hotel.image;
    this.amenities = hotel.amenities;
}

Hotel.prototype.matchesQuery = function(query) {
    if (query.name) {
        if (!this.name.toLowerCase().includes(query.name.toLowerCase())) {
            return false;
        }
    }

    if (query.stars && query.stars.length > 0 && query.stars.length != 5) {
        if (query.stars.indexOf(this.stars) == -1) {
            return false;
        }
    }

    return true;
};

module.exports = Hotel;