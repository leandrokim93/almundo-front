import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../api/hotels.service';
import { Hotel } from '../model/hotel';
import { HotelsResponse } from '../model/hotels-response';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-hotels-list',
  templateUrl: './hotels-list.component.html',
  styleUrls: ['./hotels-list.component.css']
})
export class HotelsListComponent implements OnInit {

  hotelsList: Array<Hotel>;
  currentHotelName: string;
  currentStars: Array<number>;

  constructor(private hotelsService: HotelsService, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.currentHotelName.subscribe((hotelName: string) => {
      this.currentHotelName = hotelName;
      this.getHotels(this.currentHotelName, this.currentStars);
    });

    this.dataService.currentHotelStars.subscribe((hotelStars: Array<number>) => {
      this.currentStars = hotelStars;
      this.getHotels(this.currentHotelName, this.currentStars);
    })
  }

  getHotels(hotelName: string, hotelStars: Array<number>) {
    this.hotelsService.getHotels(hotelName, hotelStars).subscribe((data: HotelsResponse) => {
      this.hotelsList = data.hotelsList;
    });
  }
}
