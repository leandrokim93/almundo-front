import { Component, OnInit, Input } from '@angular/core';
import { Hotel } from 'src/app/model/hotel';

@Component({
  selector: 'app-hotel-description',
  templateUrl: './hotel-description.component.html',
  styleUrls: ['./hotel-description.component.css']
})
export class HotelDescriptionComponent implements OnInit {

  @Input() hotel: Hotel;
  stars: Array<any>;

  constructor() { }

  ngOnInit() {
    this.stars = new Array(this.hotel.stars);
  }

}
