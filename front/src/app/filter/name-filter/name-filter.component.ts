import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";

@Component({
  selector: 'app-name-filter',
  templateUrl: './name-filter.component.html',
  styleUrls: ['./name-filter.component.css']
})
export class NameFilterComponent implements OnInit {

  showing: boolean = true;
  hotelName: string = "";

  constructor(private dataService: DataService) { }

  ngOnInit() { }

  searchHotelByName() {
    this.dataService.changeHotelName(this.hotelName);
  }

}
