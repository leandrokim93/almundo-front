# Almundo-front

Almundo front test project.

## Getting Started

### Prerequisites

It goes without saying that you need nodejs installed on your computer.

The ".env" file is in here because this is a Test App, in a real application, it should be declared in the ".gitignore" so that you don't push it to your repository and declare it in each workspace by yourself.

So you should create a .env file in the root like this:

```
PORT=3000
```

### Installing

The first step to run this is to install all missing dependencies:

```
npm install
```

And then, you can run the server:

```
node .
```

Now you can open up the browser and go into http://localhost:3000/ or the port you specified to see the home page.

## Authors

* **Leandro Kim**

## License

This project is licensed under the MIT License