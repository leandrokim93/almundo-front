//Fake Database
const hotels = require("./data.json");
const Hotel = require("../models/hotel");

module.exports.getHotels = () => {
    const hotelList = hotels.map((hotel) => {
        return new Hotel(hotel);
    });

    return hotelList;
};

module.exports.createHotel = (hotel) => {
    //Database function to create new entry
    return;
};

module.exports.updateHotel = (hotel) => {
    //Database function to update new entry
    return;
};

module.exports.deleteHotel = (hotel) => {
    //Database function to delete new entry
    return;
};