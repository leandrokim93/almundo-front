const ExpressLoader = require("./express");

module.exports = async ({ app }) => {
    await ExpressLoader({ app });

    return;
};