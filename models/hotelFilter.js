function HotelFilter(query) {
    this.name = query.name;
    this.stars = query.stars;
    if (!this.stars)
        this.stars = [];
    if (typeof this.stars === 'string')
        this.stars = [this.stars]
    this.stars = this.stars.map((star) => {
        return Number(star);
    })
}


module.exports = HotelFilter;