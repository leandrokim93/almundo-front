const Express = require("express");
const Loaders = require("./loaders");
const Config = require("./config");

async function startServer() {
    const app = Express();

    await Loaders({ app });

    app.listen(Config.port, err => {
        if (err) {
            console.log(err);
            return;
        }
        console.log("App Initialized");
    });
}

startServer();