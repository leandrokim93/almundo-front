import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-star-filter',
  templateUrl: './star-filter.component.html',
  styleUrls: ['./star-filter.component.css']
})
export class StarFilterComponent implements OnInit {

  showing: boolean = true;
  stars: Array<number> = [];
  checkedBoxes: Array<boolean> = [false, false, false, false, false];
  allChecked = true;

  constructor(private dataService: DataService) { }

  ngOnInit() { }

  checkAll() {
    for (let i = 0; i < this.checkedBoxes.length; i++) {
      this.checkedBoxes[i] = false;
    }
    this.refreshStars();
  }

  checkboxCheck() {
    for (let i = 0; i < this.checkedBoxes.length; i++) {
      if (this.checkedBoxes[i] == false) {
        this.allChecked = false;
        this.refreshStars();
        return;
      }
    }
    this.allChecked = true;
    setTimeout(() => {
      this.checkAll();
    }, 100)
  }

  refreshStars() {
    if (this.allChecked) {
      this.stars = [1, 2, 3, 4, 5];
    } else {
      this.stars = [];
      for (let i = 0; i < this.checkedBoxes.length; i++) {
        if (this.checkedBoxes[i]) {
          this.stars.push(i + 1);
        }
      }
    }
    this.dataService.changeHotelStars(this.stars);
  }

}
