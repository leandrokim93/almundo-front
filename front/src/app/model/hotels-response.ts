import { Hotel } from './hotel';

export interface HotelsResponse {
    hotelsList: Array<Hotel>;
}
