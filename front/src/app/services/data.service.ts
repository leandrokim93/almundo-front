import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private hotelNameSource = new BehaviorSubject<string>("");
  currentHotelName = this.hotelNameSource.asObservable();

  private hotelStarSource = new BehaviorSubject<Array<number>>(null);
  currentHotelStars = this.hotelStarSource.asObservable();

  constructor() { }

  changeHotelName(name: string) {
    this.hotelNameSource.next(name);
  }

  changeHotelStars(stars: Array<number>) {
    this.hotelStarSource.next(stars);
  }
}