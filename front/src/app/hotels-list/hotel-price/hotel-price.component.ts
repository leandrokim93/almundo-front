import { Component, OnInit, Input } from '@angular/core';
import { Hotel } from 'src/app/model/hotel';

@Component({
  selector: 'app-hotel-price',
  templateUrl: './hotel-price.component.html',
  styleUrls: ['./hotel-price.component.css']
})
export class HotelPriceComponent implements OnInit {
  
  @Input() hotel: Hotel;

  constructor() { }

  ngOnInit() {
  }

}
