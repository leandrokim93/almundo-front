const Express = require("express");
const Router = Express.Router();
const Path = require('path');

Router.use('/public', Express.static('public'));

Router.get("/", async (req, res) => {
    res.sendFile(Path.join(__dirname, "/../public/html/index.html"));
});

module.exports = Router;