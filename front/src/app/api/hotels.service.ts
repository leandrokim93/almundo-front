import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HotelsService {

  constructor(private http: HttpClient) { }

  getHotels(hotelName: string, hotelStars: Array<number>) {
    let params: any = {};
    if (hotelName && hotelName != "") {
      params.name = hotelName;
    }
    if (hotelStars && hotelStars.length >= 0 && hotelStars.length < 6) {
      params.stars = hotelStars;
    }
    return this.http.get("http://localhost:3000/hotels", {
      params
    });
  }

}
