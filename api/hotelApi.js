const Express = require("express");
const Router = Express.Router();
const HotelService = require("../services/hotelService");
const HotelFilter = require("../models/hotelFilter");
const Hotel = require("../models/hotel");

Router.use((req, res, next) => {
    //Possible middleware for validation
    next();
});

Router.get("/", async(req, res) => {
    const query = new HotelFilter(req.query);

    const hotelsList = await HotelService.getHotels(query);

    res.json({ hotelsList });
});

Router.post("/create", async(req, res) => {
    const hotel = new Hotel(req.body);

    await HotelService.createHotel(query);

    res.status(200).json({ status: "Hotel Created" });
});

Router.post("/update", async(req, res) => {
    const hotel = new Hotel(req.body);

    await HotelService.updateHotel(query);

    res.status(200).json({ status: "Hotel Updated" });
});

Router.post("/delete", async(req, res) => {
    const hotel = new Hotel(req.body);

    await HotelService.deleteHotel(query);

    res.status(200).json({ status: "Hotel Deleted" });
});

module.exports = Router;